﻿using System;

namespace PadawansTask1
{
    public static class Population
    {
        static void Main(string[] args)
        {
            GetYears(1500, 5, 100, 5000);
        }

        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            if (initialPopulation <= 0)
                throw new ArgumentException("Error");

            if (currentPopulation <= 0)
                throw new ArgumentException("Error");

            int year = 0;
            while (initialPopulation < currentPopulation)
            {
                initialPopulation += visitors + Convert.ToInt32(percent * initialPopulation / 100);
                year++;
            }

            return year;
        }
    }
}